# Theme Submission Instructions #

Before you can sell your theme on Themely Marketplace you must first submit your theme for review by following the steps below.

## Run Theme Check Plugin ##

The [Theme Check plugin](https://wordpress.org/plugins/theme-check/) is an easy way to test your theme and make sure it’s up to spec with the latest WordPress.org theme review standards.

## Theme Requirements ##

### style.css ###

// Unique theme name

```Theme Name: Example Pro```

// Your name or company name

```Author: Example```

// Link to your support page (support forum, support ticket or contact form)

```Author URI: https://support.example.com/```

// This description will be displayed to users in our marketplace. Limited to 250 words and can include keywords relevant to your theme. For example: Woocommerce, Ecommerce, Multipurpose, Landing Page, Corporate, Portfolio, etc.

```Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vulputate turpis ex, sed dignissim libero dapibus non. Aenean eu pharetra odio. Vivamus eu augue ac elit pharetra semper vitae quis orci. Curabitur convallis arcu at lacus porta, vel cursus felis porta. Nulla sit amet suscipit purus, sed rhoncus nulla. Vivamus porta neque eu nibh mollis, at dignissim orci aliquet. Phasellus laoreet eleifend blandit. Morbi hendrerit consequat scelerisque. Etiam odio mauris, iaculis sit amet nibh et, auctor tempus libero. Nulla convallis scelerisque orci eget venenatis. Cras maximus imperdiet ornare. Nulla interdum vel arcu at consectetur. Ut consequat sapien eu massa placerat, ac varius metus aliquam. Curabitur luctus lectus non ullamcorper rutrum. In nibh orci, commodo at arcu eu, tristique sollicitudin mi. Nunc fermentum neque dolor, vel fringilla massa volutpat a. Aenean ut elit condimentum, maximus libero nec, suscipit orci. Aliquam pellentesque velit quam, eget molestie nibh vulputate proin eleifend gravida elit.```

// Set current theme version

```Version: 1.0.0```

// Set theme license (GNU GPL v2.0 or Themely License)

```License: GNU General Public License v2 or later``` or ```License: Themely License``` 

// Link to license or include Themely license.md file

```License URI: http://www.gnu.org/licenses/gpl-2.0.html``` or ```License URI: license.md```

// Unique text-domain (cannot conflict with WordPress.org themes)

```Text Domain: text-domain```

// Set **tested up to** version

```Tested up to: 5.8```

// Set minimum PHP version

```Requires PHP: 5.6```

### screenshot.png ###

Theme ```screenshot.png``` image file must be 1200px in width and 900px in height and less than 1MB in size.

### screenshot-long.png ###

Include a full-length screenshot with the following name ```screenshot-long.png``` in the root directory of your theme. The full-length screenshot image can also be a sales page depicting theme features. The image file must be 800px in width and maximum 12000px in height.

## Theme Onboarding (Optional) ##

You can opt-in to use our [Theme Setup Wizard](https://bitbucket.org/ismaelyws/themely-marketplace-onboarding/) to speed up and simplify the onboarding experience for your new customers.

## Create Private Git Repository ##

Create a private Github or Bitbucket repository with the following name: ```Theme Name Themely Marketplace```

Example: ```Serenity Pro Themely Marketplace```

## Enable Issue Tracker ##

**Enabling the Issue Tracker in Bitbucket**

1. From a repository, click Settings.
2. Select Issue tracker from the left-hand tabs.
3. Check the option for Private issue tracker.The system saves your selection and the Issue Tracker item appears in the repository's menu bar. 

**Enabling the Issue Tracker in Github**

Issue tracker is enabled by default in Github

## Grant Review Team Read Access ##

Add our Review Team as read-only user to your private repository.

Name: ```Themely Marketplace Review Team```

Email: ```reviewteam@themely.com```

Github Username: ```themely-marketplace```

Bitbucket Username: ```themely``` 

For Bitbucket read [Grant repository access to users and groups](https://support.atlassian.com/bitbucket-cloud/docs/grant-repository-access-to-users-and-groups/)

For Github read [Inviting collaborators to a personal repository](https://docs.github.com/en/account-and-profile/setting-up-and-managing-your-github-user-account/managing-access-to-your-personal-repositories/inviting-collaborators-to-a-personal-repository)

## Theme Review Process ##

Once you add our Review Team to your private repository we'll be notified via email and will begin the review process. Submitting a theme for review DOES NOT guarantee inclusion to our marketplace. Themes can be rejected if they do not meet our quality guidelines. You can expect to receive a response within 3-5 business days after submitting your theme. If you have received an invitation to submit your themes then we have already determined your themes pass our quality guidelines and the review process will be quicker.

## Questions or Technical Support ##

If you have questions about the submission instructions or review process here's how you can reach us.

[Chat with us on Discord](https://discord.gg/f3m2Pmp)

Send an email to `reviewteam@themely.com`

Voice message or text on Whatsapp `+1 (514) 883-0132`

Time Zone: Eastern Standard Time (GMT -4)

Spoken & written languages: English, Français, Español (un poquito)

Office Location: Montreal, Canada